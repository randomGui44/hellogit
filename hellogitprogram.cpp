#include <iostream>
#include <vector>

using namespace std;

int main(){

	vector<char> word = {'w', 'o', 'r', 'd'};
	
	cout<<"Vector before push_back: ";
	for(int counter = 0; counter < word.size(); counter++){
		cout<<word[counter]<<" ";
	}//End For
	
	word.push_back('s');
	cout<<endl<<endl;
	
	cout<<"Vector after push_back: ";
	for(int counter = 0; counter < word.size(); counter++){
		cout<<word[counter]<<" ";
	}//End For
	
	word.pop_back();
	cout<<endl<<endl;
	
	cout<<"Vector after pop_back: ";
	for(auto letter:word){
		cout<<letter<<" ";
	}//End For
	
	cout<<endl<<endl;
	return 0;
}//end Main
